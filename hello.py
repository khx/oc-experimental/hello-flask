from textwrap import dedent

from flask import Flask

application = Flask(__name__)


@application.route("/")
def root():
    template = dedent(
        """\
        <!DOCTYPE html>
        <html lang="en-us">
            <head>
                <title>{title}</title>
            </head>
            <body>
                <h1>{title}</h1>
            </body>
        </html>
        """
    )

    return template.format(title="Hello, Flask!")
