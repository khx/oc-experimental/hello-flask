from setuptools import setup

setup(
    name="hello",
    version="0.1.0",
    description="Test Flask app for OpenShift s2i",
    py_modules=["hello"],
    install_requires=["gunicorn", "Flask"],
)
